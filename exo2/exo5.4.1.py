#afficher en utilisant for

animaux=["vache" , "souris" , "levure" , "bacterie"]
for i in animaux:
    print(i)
    
#afficher en utilisant while
animal=["vache" , "souris" ,"levure" ,"bacterie"]
i=0
#revoye la sequence 
while i < len(animal):
    print(animal[i])
    i += 1   
    
#en utilisant encore pour 

tableau = ["vache", "souris", "levure", "bacterie"]

# Boucle for
for i in range(len(tableau)):
    print(tableau[i])
    